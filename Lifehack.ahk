﻿Class SurfaceProLifeHacker
{
	Setup()
	{
		Menu, Tray, Icon, %A_ScriptDir%\teogai.ico 
		Menu, Tray, NoStandard
		Menu, Tray, Add, Mute Background Apps, SurfaceProLifeHacker.MuteBackgroundApps
		Menu, Tray, Add, Reload, SurfaceProLifeHacker.Reload
		Menu, Tray, Add, Exit, SurfaceProLifeHacker.ExitScript
		Menu, Tray, Tip, Teogai Lifehacker		

		parsecVolume := new AudioController(["ParsecMinFrame"])
		parsecVolume.Init(0.1)
		parsecVolume.Start()
	}

	MuteBackgroundApps()
	{
		muteBackgroundApps := new AudioController(["Genshin Impact"])
		muteBackgroundApps.Init(-1)
		muteBackgroundApps.Start()
	}

	Reload()
	{
		Reload
	}
	
	ExitScript()
	{
		ExitApp
	}
	
	CheckAdmin()
	{
		if not A_IsAdmin
		{
   			Run *RunAs "%A_ScriptFullPath%"  ; Requires v1.0.92.01+
   			ExitApp
		}
	}
}

;;SurfaceProLifeHacker.CheckAdmin()
SurfaceProLifeHacker.Setup()

#Include Scripts/HotKey.ahk
#Include Scripts/UI.ahk
#Include Scripts/AudioController.ahk

#NoEnv
#SingleInstance, Force
#Persistent
#InstallKeybdHook
#UseHook
#KeyHistory, 0
#HotKeyInterval 1
#MaxHotkeysPerInterval 127
