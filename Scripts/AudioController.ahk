﻿#Persistent
#Include Scripts/VistaAudioControlFunctions.ahk

class AudioController 
{
    __New(windowsTitles) 
    {
        this.interval := 500
        this.count := 0
        this.windowTitles := windowsTitles
        ; Tick() has an implicit parameter "this" which is a reference to
        ; the object, so we need to create a function which encapsulates
        ; "this" and the method to call:
        this.isActive := true
    }

    Init(volume)
    {
        if(volume >= 0)
        {
            this.volume := volume
            this.timer := ObjBindMethod(this, "SetVolume")
        }
        else
        {
            this.timer := ObjBindMethod(this, "Tick")
        }

    }

    Start() 
    {
        ; Known limitation: SetTimer requires a plain variable reference.
        timer := this.timer
        SetTimer % timer, % this.interval
    }

    Stop() 
    {
        ; To turn off the timer, we must pass the same object as before:
        timer := this.timer
        SetTimer % timer, Off
    }
    
    Toggle() 
    {
        this.isActive := !this.isActive
    }

    ; In this example, the timer calls this method:
    Tick() 
    {
        if(!this.isActive)
            return

        ; ToolTip % ++this.count
        For i, windowTitle in this.windowTitles
        {
            WinGet, windowPID, PID, % windowTitle
            if (windowPID)
            {
                if (VolumeObject := this.GetVolumeObject(windowPID))
                {
                   if WinActive(windowTitle)
                        VA_ISimpleAudioVolume_SetMute(VolumeObject, false)
                    else
                        VA_ISimpleAudioVolume_SetMute(VolumeObject, true)
                        
                    ObjRelease(VolumeObject)
                }
            }
        }
    }
 
    GetVolumeObject(Param)
    {
        static IID_IASM2 := "{77AA99A0-1BD6-484F-8BC7-2C654C9A9B6F}"
        , IID_IASC2 := "{bfb7ff88-7239-4fc9-8fa2-07c950be9c6d}"
        , IID_ISAV := "{87CE5498-68D6-44E5-9215-6DA47EF883D8}"
        

        ; Turn empty into integer
        if !Param
            Param := 0
        
        ; Get PID from process name
        if Param is not Integer
        {
            Process, Exist, %Param%
            Param := ErrorLevel
        }

        ; GetDefaultAudioEndpoint
        DAE := VA_GetDevice()
        
        ; activate the session manager
        VA_IMMDevice_Activate(DAE, IID_IASM2, 0, 0, IASM2)
        
        ; enumerate sessions for on this device
        VA_IAudioSessionManager2_GetSessionEnumerator(IASM2, IASE)
        VA_IAudioSessionEnumerator_GetCount(IASE, Count)
        
        ; search for an audio session with the required name
        Loop, % Count
        {
            ; Get the IAudioSessionControl object
            VA_IAudioSessionEnumerator_GetSession(IASE, A_Index-1, IASC)
            
            ; Query the IAudioSessionControl for an IAudioSessionControl2 object
            IASC2 := ComObjQuery(IASC, IID_IASC2)
            ObjRelease(IASC)
            
            ; Get the sessions process ID
            VA_IAudioSessionControl2_GetProcessID(IASC2, SPID)
            
            ; If the process name is the one we are looking for
            if (SPID == Param)
            {
                ; Query for the ISimpleAudioVolume
                ISAV := ComObjQuery(IASC2, IID_ISAV)
                
                ObjRelease(IASC2)
                break
            }
            ObjRelease(IASC2)
        }
        ObjRelease(IASE)
        ObjRelease(IASM2)
        ObjRelease(DAE)
        return ISAV
    }
    
    SetVolume()
    {
        ; ToolTip % ++this.count
        For i, windowTitle in this.windowTitles
        {
            WinGet, windowPID, PID, % windowTitle
            if (windowPID)
            {
                if (VolumeObject := this.GetVolumeObject(windowPID))
                {
                    VA_ISimpleAudioVolume_SetMasterVolume(VolumeObject, this.volume)
                    ObjRelease(VolumeObject)
                }
            }
        }
    }
    
    ;
    ; ISimpleAudioVolume : {87CE5498-68D6-44E5-9215-6DA47EF883D8}
    ;
    
}

VA_ISimpleAudioVolume_SetMasterVolume(this, ByRef fLevel, GuidEventContext="") {
    return DllCall(NumGet(NumGet(this+0)+3*A_PtrSize), "ptr", this, "float", fLevel, "ptr", VA_GUID(GuidEventContext))
}
VA_ISimpleAudioVolume_GetMasterVolume(this, ByRef fLevel) {
    return DllCall(NumGet(NumGet(this+0)+4*A_PtrSize), "ptr", this, "float*", fLevel)
}
VA_ISimpleAudioVolume_SetMute(this, ByRef Muted, GuidEventContext="") {
    return DllCall(NumGet(NumGet(this+0)+5*A_PtrSize), "ptr", this, "int", Muted, "ptr", VA_GUID(GuidEventContext))
}
VA_ISimpleAudioVolume_GetMute(this, ByRef Muted) {
    return DllCall(NumGet(NumGet(this+0)+6*A_PtrSize), "ptr", this, "int*", Muted)
}