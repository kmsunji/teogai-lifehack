BS := new BrightnessAndVolumeSetter()

;;^ => Ctrl
;;! => Alt
;;+ => Shift
;;# => Win

^!LButton::RButton
^!+LButton::MButton

^!+F1::PowerPlanManager.SetBalancedPlan()
^!+F2::PowerPlanManager.SetHighPerformancePlan()
^!+F3::PowerPlanManager.SetPowerSaverPlan()
;^!+F4::
^!+F5::Reload
;^!+F6::MonitorManager.ChangeResolution(800, 450)	
^!+F7::Run %ComSpec% /c %A_ScriptDir%."\Scripts\Bluetooth\connect.bat" 
^!+F8::Run %ComSpec% /c %A_ScriptDir%."\Scripts\Bluetooth\disconnect.bat"
^!+F9::
^!+F10::
^!+F11::SetTimer, Genshin, 10
^!+F12:: 
SetTimer, Genshin, Off
Run netsh advfirewall firewall set rule name="Block Genshin" new enable=no, , Hide
Return
Genshin()
{
    SoundPlay, *-1
    Loop 6
    {
        Run netsh advfirewall firewall set rule name="Block Genshin" new enable=yes, , Hide
        Random, rand, 2000, 2500
        Sleep %rand%
        Run netsh advfirewall firewall set rule name="Block Genshin" new enable=no, , Hide
        Random, rand, 200, 300
        Sleep %rand%
    }
    SoundPlay, *16
    Run netsh advfirewall firewall set rule name="Block Genshin" new enable=yes, , Hide
    Random, rand, 2000, 1200
    Sleep %rand%
    Run netsh advfirewall firewall set rule name="Block Genshin" new enable=no, , Hide
    Random, rand, 200, 300
    Sleep %rand%
    Sleep 1500
}
^!+A::WinSet, AlwaysOnTop, ,A
;^!+B::
;^!+C::
^!+D::Run desk.cpl
^!+E::Run explorer.exe
;^!+F::
;!+G::
;^!+H::
;^!+I::
;^!+J::
;^!+K::
;^!+L::
;^!+M::
;^!+N::
;^!+O::
;^!+P::
;^!+Q::
^!+R::Run regedit
^!+S::Run shell:Startup
;^!+T::
;^!+U::
;^!+V::
;^!+W::
;^!+X::
;^!+Y::
;^!+Z::

;#A:: --Action Center
;#B::
#C::Launcher.LaunchPowershellAtCurrentDir()
; #+C::Launcher.LaunchPowershellAtCurrentDir(true)
;#D:: --Show Desktop
;#E:: --Explorer
;#F::
;#G::
#H::
    GitProjectsVar := EnvironmentManager.GetVariable("GitProjects")
    Run %GitProjectsVar%
Return 
;#I:: --Settings
#J::
    DownloadsVar := EnvironmentManager.GetVariable("Downloads")
    Run %DownloadsVar%
Return
;#K::
;#L:: --Lock
;#M:: --Minimize
;#N::
;#P::
;#Q::
;#R:: --Run
;#S::
;#T:: --Tab
;#U::
#V:: Launcher.LaunchVSCodeAtCurrentDir()
;#W:: --Ink Workspace
;#X:: --System Apps
;#Y::
;#Z::

^!+Up::Run C:\Users\kamin\Downloads\display64.exe /rotate:0
^!+Left::Run C:\Users\kamin\Downloads\display64.exe /rotate:90
^!+Down::Run C:\Users\kamin\Downloads\display64.exe /rotate:180
^!+Right::Run C:\Users\kamin\Downloads\display64.exe /rotate:270

^!+/::Run SnippingTool.exe

#Backspace::CtrlBreak ;Make Win+Backspace = Pause/

^!+WheelUp::BS.SetBrightness(1)
^!+WheelDown::BS.SetBrightness(-1)

#IfWinActive
^!WheelUp::BS.SetVolume(1)
^!WheelDown::BS.SetVolume(-1)
^!MButton::Send {Blind}{Volume_Mute}
return

#F20::return ;Single Pen Click
#F19::Run "C:\Program Files\Adobe\Adobe Photoshop CC 2017\Photoshop.exe" ;Double Pen Click
#F18::return ;Pen Click and Hold

#Include Scripts/PowerPlanManager.ahk
#Include Scripts/MonitorManager.ahk
#Include Scripts/Launcher.ahk
#Include Scripts/EnvironmentManager.ahk
#Include Scripts/BrightnessAndVolumeSetter.ahk

$F7:: Send ^V

